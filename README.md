## renren-fast-vue

- renren-fast-vue 基于 vue、element-ui 构建开发，实现[renren-fast](https://gitee.com/renrenio/renren-fast)后台管理前端功能，提供一套更优的前端解决方案
- 前后端分离，通过 token 进行数据交互，可独立部署
- 主题定制，通过 scss 变量统一一站式定制
- 动态菜单，通过菜单管理统一管理访问路由
- 数据切换，通过 mock 配置对接口数据／mock 模拟数据进行切换
- 发布时，可动态配置 CDN 静态资源／切换新旧版本
- 演示地址：[http://demo.open.renren.io/renren-fast](http://demo.open.renren.io/renren-fast) (账号密码：admin/admin)

![输入图片说明](https://images.gitee.com/uploads/images/2019/0305/133529_ff15f192_63154.png "01.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0305/133537_7a1b2d85_63154.png "02.png")

## 说明文档

项目开发、部署等说明都在[wiki](https://github.com/renrenio/renren-fast-vue/wiki)中。

## 更新日志

每个版本的详细更改都记录在[release notes](https://github.com/renrenio/renren-fast-vue/releases)中。

## 环境要求

nodejs <= 11.5

cnpm i id-validator --save

## vscode 常用插件

Auto Close Tag
Auto Rename Tag
Chinese(Simplified)
ESLint
HTML CSS Support
HTML Snippets
Javascript(ES6)
Live Server
open in browser
Vetur

## vscode setting.json 配置

```
{
  "editor.fontFamily": "DejaVu Sans Mono, 'Courier New', monospace, Consolas",
  "editor.fontSize": 18,
  "workbench.colorTheme": "Night Owl",
  "terminal.integrated.automationShell.windows": "",
  "terminal.integrated.shell.windows": "D:\\Program Files\\Git\\bin\\bash.exe",
  "editor.renameOnType": true,
  // "[javascript]": {
  //     "editor.defaultFormatter": "HookyQR.beautify"
  // },
  // "files.autoSave": "afterDelay",
  "eslint.codeAction.showDocumentation": {
    "enable": true
  },
  "editor.defaultFormatter": "esbenp.prettier-vscode", //编辑器格式化工具
  "[javascript]": {
    "editor.defaultFormatter": "rvest.vs-code-prettier-eslint"
  }, //javascript格式化工具
  "[vue]": {
    "editor.defaultFormatter": "octref.vetur"
  }, //vue格式化工具
  "editor.insertSpaces": false,
  "workbench.editor.enablePreview": false, //打开文件不覆盖
  "search.followSymlinks": false, //关闭rg.exe进程
  "editor.minimap.enabled": true, //关闭快速预览
  "files.autoSave": "afterDelay", // 编辑自动保存
  "editor.lineNumbers": "on", // 开启行数提示
  "editor.quickSuggestions": {
    //开启自动显示建议
    "other": true,
    "comments": true,
    "strings": true
  },
  "editor.tabSize": 2, //制表符符号eslint
  "editor.formatOnSave": true, //每次保存自动格式化
  // "eslint.codeActionsOnSave": {
  //     "source.fixAll.eslint": true
  // },
  "prettier.eslintIntegration": true, //让prettier使用eslint的代码格式进行校验
  "prettier.semi": true, //去掉代码结尾的分号
  "prettier.singleQuote": false, //使用单引号替代双引号
  "javascript.format.insertSpaceBeforeFunctionParenthesis": true, //让函数(名)和后面的括号之间加个空格
  "vetur.format.defaultFormatter.html": "js-beautify-html", //格式化.vue中html
  "vetur.format.defaultFormatter.js": "vscode-typescript", //让vue中的js按编辑器自带的ts格式进行格式化
  "vetur.format.defaultFormatterOptions": {
    "js-beautify-html": {
      "wrap_attributes": "force-aligned" //属性强制折行对齐
    },
    "prettier": {
      "semi": false,
      "singleQuote": true
    },
    "vscode-typescript": {
      "semi": false,
      "singleQuote": true
    }
  },
  "eslint.validate": [
    "vue",
    // "javascript",
    "typescript",
    "typescriptreact",
    "html"
  ],
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  }
}

```

docker run -d -p 9000:9000 \
--name php74 \
-v /data/www:/var/www/html \
-v /data/php:/usr/local/etc/php \
--link mysql57:mysql57 \
--link redis608:redis608 \
--privileged=true \
php:7.4.11-fpm

docker run --name nginx \
-p 8000-8088:8000-8088 -p 80:80 \
-v /data/www:/usr/share/nginx/html \
-v /data/nginx/conf:/etc/nginx \
-v /data/nginx/logs:/var/log/nginx \
--link php74:php74 \
--privileged=true \
-d nginx:1.19.3
